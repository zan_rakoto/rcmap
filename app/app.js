'use strict';
angular.module('rcMap', []);
angular.module('rcMapDemo', ['ngRoute', 'rcMap'])
    .config(function ($routeProvider) {
        $routeProvider

            .when('/', {
                templateUrl: 'main/main.tpl.html',
                controller: 'MainCtrl'
            })

            .when('/about', {
                templateUrl: 'about/about.tpl.html',
                controller: 'AboutCtrl'
            })

            .otherwise({
                redirectTo: '/'
            });
    });

Math.degToRad = function(deg) {
    return deg * Math.PI / 180;
};

Math.radToDeg = function(rad) {
    return rad * 180 / Math.PI;
};
