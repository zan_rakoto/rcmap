'use strict';

angular.module('rcMap', []);
angular.module('rcMapDemo', ['ngRoute', 'rcMap'])
    .config(function ($routeProvider) {
        $routeProvider

            .when('/', {
                templateUrl: 'main/main.tpl.html',
                controller: 'MainCtrl'
            })

            .when('/about', {
                templateUrl: 'about/about.tpl.html',
                controller: 'AboutCtrl'
            })

            .when('/demos', {
                templateUrl: 'demos/demos.tpl.html',
                controller: 'DemosCtrl'
            })

            .when('/demos/draggable', {
                templateUrl: 'demos/draggable.tpl.html',
                controller: 'DraggableCtrl'
            })

            .when('/demos/draganddrop', {
                templateUrl: 'demos/draganddrop.tpl.html',
                controller: 'DragAndDropCtrl'
            })

            .otherwise({
                redirectTo: '/'
            });
    });
