'use strict';

/**
 * @ngdoc function
 * @name rcMapDemo.main.MainCtrl
 * @description
 * # MainCtrl
 * Controller of the senrcDemo main page
 */
angular.module('rcMapDemo')
    .controller('MainCtrl', ['$scope', function ($scope) {
        $scope.date = new Date();
    }]);
