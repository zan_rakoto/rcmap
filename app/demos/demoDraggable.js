'use strict';

angular.module('rcMapDemo')
    .constant('Hammer', Hammer)
    .constant('hammerConfig', {
        transformMinScale: 1,
        transformMinRotation: 0,
        dragBlockHorizontal: true,
        dragBlockVertical: true,
        dragMinDistance: 0
    })
    .directive('demoDraggable', ['$document', 'Hammer', 'hammerConfig', function ($document, Hammer, hammerConfig) {
        return{
            restrict: 'A',
            link: function (scope, element) {

                var posX = 0, posY = 0,
                    lastPosX = 0, lastPosY = 0,
                    scale = 1, lastScale,
                    rotation = 0, lastRotation;

                var hammerObj = new Hammer(element[0], hammerConfig);

                hammerObj.on('touch drag dragend transform', function (ev) {
                    switch (ev.type) {
                        case 'touch':
                            lastScale = scale;
                            lastRotation = rotation;
                            break;

                        case 'drag':
                            posX = ev.gesture.deltaX + lastPosX;
                            posY = ev.gesture.deltaY + lastPosY;
                            break;

                        case 'transform':
                            rotation = lastRotation + ev.gesture.rotation;
                            scale = Math.max(1, Math.min(lastScale * ev.gesture.scale, 10));
                            break;

                        case 'dragend':
                            lastPosX = posX;
                            lastPosY = posY;
                            break;
                    }

                    var transform =
                        "translate(" + posX + "px," + posY + "px) " +
                            "scale(" + scale + ") " +
                            "rotate(" + rotation + "deg) ";

                    element.css({
                        transform: transform,
                        oTransform: transform,
                        msTransform: transform,
                        mozTransform: transform,
                        webkitTransform: transform
                    });

                });
            }
        }
    }
    ])
;
