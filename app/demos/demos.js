'use strict';

/**
 * @ngdoc function
 * @name rcMapDemo.about.AboutCtrl
 * @description
 * # MainCtrl
 * Controller of the rcMapDemo about page
 */
angular.module('rcMapDemo')
    .controller('DemosCtrl', ['$scope', function ($scope) {
        //Empty Ctrl for demos
        $scope.date = new Date();
    }])
    .controller('DraggableCtrl', ['$scope', function ($scope) {
        //Empty Ctrl for demos
        $scope.date = new Date();
    }])
    .controller('DragAndDropCtrl', ['$scope', function ($scope) {
        //Empty Ctrl for demos
        $scope.date = new Date();
    }]);
