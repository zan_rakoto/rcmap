'use strict';

/**
 * @ngdoc function
 * @name rcMapDemo.about.AboutCtrl
 * @description
 * # MainCtrl
 * Controller of the rcMapDemo about page
 */
angular.module('rcMapDemo')
    .controller('AboutCtrl', ['$scope', function ($scope) {
        $scope.date = new Date();
    }]);
