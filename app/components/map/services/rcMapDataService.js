'use strict';

angular.module('rcMap')
    .factory('rcMapDataService', ['rcUtilService', '$log', '$q', function (rcUtilService, $log, $q) {

        var data = {
            "wallpaper": "images/rep/sites.svg",
            name: "Top Level",
            "sites": [
                {
                    "name": "Site1",
                    "position": {
                        "x": 50,
                        "y": 125
                    },
                    link: "http://www.robusta.io/",
                    color: {
                        front: "white",
                        back: "blue"
                    },
                    "tags": [
                        {
                            "name": "Site important",
                            "position": {"x": 250, "y": 35}
                        },
                        {
                            "name": "Source de la clim",
                            "position": {"x": 150, "y": 335}
                        }
                    ],
                    "buildings": [
                        {
                            "name": "Bat1",
                            "position": {
                                "x": 50,
                                "y": 45
                            },
                            "floors": [
                                {
                                    "name": "Etage0",
                                    "wallpaper": "images/rep/floor.svg",
                                    "position": {
                                        "x": 0,
                                        "y": 0
                                    },
                                    color: {
                                        front: "red",
                                        back: "yellow"
                                    },
                                    "tags": [
                                        {
                                            "name": "Bureau de Nicolas",
                                            "position": {"x": 25, "y": 35}
                                        }
                                    ],
                                    "rooms": [
                                        {
                                            "name": "BureauA",
                                            "wallpaper": "images/rep/roomA.svg",
                                            "position": {
                                                "x": 2,
                                                "y": 4
                                            },
                                            "nodes": [
                                                {
                                                    "name": "Copieur",
                                                    "position": {
                                                        "x": 345,
                                                        "y": 78
                                                    },
                                                    link: "http://www.robusta.io/products/9494781197"
                                                },
                                                {
                                                    "name": "5F6AAR",
                                                    "position": {
                                                        "x": 25,
                                                        "y": 218
                                                    },
                                                    link: "http://www.robusta.io/products/9494752397"
                                                }
                                            ]
                                        },
                                        {
                                            "name": "BureauB",
                                            "wallpaper": "images/rep/roomB.svg",
                                            "position": {
                                                "x": 2,
                                                "y": 4
                                            },
                                            "nodes": [
                                                {
                                                    "name": "5E4444",
                                                    "position": {
                                                        "x": 410,
                                                        "y": 78
                                                    },
                                                    link: "http://www.robusta.io/products/9494749897"
                                                },
                                                {
                                                    "name": "5E8123"
                                                }
                                            ]
                                        },
                                        {
                                            "name": "BureauAbis",
                                            "wallpaper": "images/rep/roomA.svg",
                                            "nodes": [
                                                {
                                                    "name": "5E8127",
                                                    "position": {
                                                        "x": 45,
                                                        "y": 78
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "name": "Etage1",
                                    "wallpaper": "images/rep/floor.svg",
                                    "position": {
                                        "x": 300,
                                        "y": 350
                                    },
                                    "tags": [
                                        {
                                            "name": "Bureau de Julien",
                                            "position": {"x": 25, "y": 55}
                                        }
                                    ],
                                    "rooms": [
                                        {
                                            "name": "Bureau Julien  ",
                                            "wallpaper": "images/rep/roomA.svg",
                                            "position": {
                                                "x": 200,
                                                "y": 45
                                            },
                                            "nodes": [
                                                {
                                                    "name": "Lumiere",
                                                    "position": {
                                                        "x": 345,
                                                        "y": 78
                                                    }
                                                },
                                                {
                                                    "name": "KKJDAR",
                                                    "position": {
                                                        "x": 25,
                                                        "y": 218
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "name": "Etage3",
                                    "position": {
                                        "x": 200,
                                        "y": 150
                                    },
                                    "tags": [
                                        {
                                            "name": "Entrée",
                                            "position": {"x": 35, "y": 35}
                                        }
                                    ],
                                    "rooms": [
                                        {
                                            "name": "Bureau AB1",
                                            "position": {
                                                "x": 200,
                                                "y": 45
                                            },
                                            "nodes": [
                                                {
                                                    "name": "Lumiere",
                                                    "position": {
                                                        "x": 345,
                                                        "y": 78
                                                    }
                                                },
                                                {
                                                    "name": "Capteur",
                                                    "position": {
                                                        "x": 25,
                                                        "y": 218
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "name": "Bat1 Bis",
                            "position": {
                                "x": 150,
                                "y": 145
                            },
                            "floors": [
                                {
                                    "name": "Bat1 Bis-RDC",
                                    "position": {
                                        "x": 50,
                                        "y": 45
                                    }
                                },
                                {
                                    "name": "Bat1 Bis-1er"
                                }
                            ]
                        }
                    ]
                },
                {
                    name: "site B",
                    link: "http://www.angularjs.org",
                    "buildings": [
                        {
                            "name": "Batiment SiteB",
                            "position": {
                                "x": 350,
                                "y": 45
                            }
                        }
                    ]
                }
            ]
        };
        var sites,
            nodes,
            tags,
            dataResult = {},
            levels;

        var findNodes = function (obj) {
            var memo = [],
                lastRoom;

            function iterator(value) {
                var type = Object.prototype.toString.call(value);
                if (type === '[object Array]') {
                    for (var i = 0, len = value.length; i < len; i++) {
                        iterator(value[i]);
                    }
                } else if (type === '[object Object]') {
                    for (var key in value) {
                        if (key === 'nodes') {
                            var v = value[key];
                            for (var k = 0, kLen = v.length; k < kLen; k++) {
                                v[k].parentName = value.name || '';
                            }
                            memo = memo.concat(v);
                            break;
                        }
                        iterator(value[key]);
                    }
                }
                return memo;
            }

            return iterator(obj);
        }

        var init = function (d) {
            var parentName;
            dataResult = rcUtilService.deepReduce(d, function (memo, value, path) {

                if (angular.isString(value)) {
                    return memo;
                }

                var key = 'root';
                if (path.length > 0) {
                    key = path[path.length - 1].k;
                }
                if (path.length > 1) {
                    for (var i = path.length - 2; i >= 0; i--) {
                        if (angular.isString(path[i].k)) {
                            parentName = path[i].k;
                            break;
                        }
                    }
                }

                var keys = ['sites', 'buildings', 'floors', 'rooms', 'nodes'];

                /*if (key === 'root') {
                 memo.levels.push({elems: value.sites, elemsType: 'sites', obj: value, type: 'root', name: value.name});
                 }*/

                if (keys.indexOf(key) !== -1) {
                    var obj = path[path.length - 1].v;
                    var nodes = findNodes(obj);
                    var level = {elems: value, elemsType: key, obj: obj, type: parentName || 'root', name: obj.name};
                    level.nodes = nodes;
                    level.path = path.reduce(function (memo, current, index, array) {
                        if (current.v.name)
                            memo.push(current.v.name);
                        return memo;
                    }, []);
                    memo.levels.push(level);
                }

                if (key === 'sites' || key === 'buildings' || key === 'floors' || key === 'sites' || key === 'rooms') {
                    memo.elems.sites = memo.elems.sites.concat(value.map(
                        function (v) {
                            return {name: v.name, type: key, isSet: angular.isDefined(v.position), position: v.position, link: v.link, color: v.color, wallpaper: v.wallpaper}
                        }));
                }
                if (key === 'nodes') {
                    memo.elems.nodes = memo.elems.nodes.concat(value);
                }
                if (key === 'tags') {
                    memo.elems.tags = memo.elems.tags.concat(value);
                }

                return memo;

            }, {elems: {sites: [], nodes: [], tags: []}, levels: []});

            levels = dataResult.levels;
            //$log.log(dataResult);
        }

        init(data);
        if (dataResult && dataResult.elems) {
            sites = dataResult.elems.sites;
            nodes = dataResult.elems.nodes;
            tags = dataResult.elems.tags;
        }

        return {
            getMapData: function () {
                return data;
            },
            getSites: function () {
                var properties = ['name', 'position', 'link', 'color'];
                var dataResult = [];
                angular.forEach(sites, function (value) {
                    var site = {};
                    angular.forEach(value, function (value2, key2) {
                            if (properties.indexOf(key2) !== -1) {
                                site[key2] = value2;
                            }
                        },
                        this
                    );
                    this.push(site);
                }, dataResult);
                return dataResult;
            },
            getElementsForAction: function () {
                if (dataResult) {
                    return dataResult.elems;
                }
            },
            getSiteInfoByName: function (siteName) {
                var siteInfo = {};
                angular.forEach(sites, function (value) {
                    if (value.name === siteName) {
                        siteInfo = value;
                    }
                })
                return siteInfo;
            },
            getNodeInfoByName: function (nodeName) {
                var nodeInfo = {};
                angular.forEach(nodes, function (value) {
                    if (value.name === nodeName) {
                        nodeInfo = value;
                    }
                })
                return nodeInfo;
            },
            getTagInfoByName: function (tagName) {
                var tagInfo = {};
                angular.forEach(sites, function (value) {
                    if (value.name === tagName) {
                        tagInfo = value;
                    }
                })
                return tagInfo;
            },
            getLevelByName: function (name) {
                var level;
                for (var i = 0; i < levels.length; i++) {
                    var value = levels[i];
                    if (value.name.trim() === name.trim()) {
                        level = value;
                        break;
                    }
                }
                return level;
            },
            getRootLevel: function () {
                var level;
                for (var i = 0; i < levels.length; i++) {
                    var value = levels[i];
                    if (value.type === 'root') {
                        level = value;
                        break;
                    }
                }
                return level;
            }
        };
    }])
;