'use strict';

angular.module('rcMap')
    .factory('rcUtilService', function () {
        return {
            /**
             * https://gist.github.com/JamieMason/4be6028de0608d5854c0
             */
            deepReduce: function (collection, fn, memo) {

                function iterator(value, path, iOrKey) {
                    var type = Object.prototype.toString.call(value);
                    memo = fn(memo, value, path, iOrKey);

                    if (type === '[object Array]') {
                        for (var i = 0, len = value.length; i < len; i++) {
                            iterator(value[i], path.concat({k:i, v:value}), i);
                        }
                    } else if (type === '[object Object]') {
                        for (var key in value) {
                            iterator(value[key], path.concat({k:key, v:value}), key);
                        }
                    }
                    return memo;
                }

                return iterator(collection, []);

            }
        };
    });