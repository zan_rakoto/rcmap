'use strict';

angular.module('rcMap')
    .factory('rcDragAndDropService', ['$q', '$log', function ($q, $log) {
        var droppables = [];

        var Droppable = function ($el) {
            this.d = $q.defer();
            this.$el = $el;
            this.isBeingDraggedOver = false;
            this.promise = this.d.promise;
        }

        Droppable.prototype = {
            contains: function (point) {
                var rect = this.$el[0].getBoundingClientRect();

                return (
                    rect.left < point.x
                        && rect.top < point.y
                        && point.x < rect.right
                        && point.y < rect.bottom
                    );
            },
            onDragEnter: function (data) {
                this.isBeingDraggedOver = true;
                this.d.notify({type: 'enter', data: data});
            },
            onDragDrop: function (data) {
                this.d.notify({type: 'drop', data: data});
            },
            onDragExit: function (data) {
                this.isBeingDraggedOver = false;
                this.d.notify({type: 'exit', data: data});
            }
        }

        return {
            registerDroppable: function ($el) {
                var droppable = new Droppable($el);
                droppables.push(droppable);
                return droppable.promise;
            },
            removeRegisteredDroppable: function ($el) {
                //TODO or TO REMOVE
            },
            registerDragMouvement: function (data) {
                for (var i = 0; i < droppables.length; i++) {
                    if (droppables[i].contains(data) && !droppables[i].isBeingDraggedOver) {
                        droppables[i].onDragEnter(data);
                    } else if (!droppables[i].contains(data) && droppables[i].isBeingDraggedOver) {
                        droppables[i].onDragExit(data);
                    }
                }
            },
            registerDrop: function (data) {
                var d = $q.defer();
                var droppable;
                var droppedInDroppable = false;
                for (var j = 0; j < droppables.length; j++) {
                    if (droppables[j].isBeingDraggedOver) {
                        droppable = droppables[j];
                        droppable.onDragDrop(data);
                        droppedInDroppable = true;
                    }
                }
                if(droppedInDroppable && droppable)
                    d.resolve(droppable.$el);
                else
                    d.reject();
                return d.promise;
            }
        };
    }]);