'use strict';

angular.module('rcMap')
    .directive('rcMapSite', [ 'rcMapDataService', '$window', '$compile', '$log', function (rcMapDataService, $window, $compile, $log) {
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'components/map/directives/sites/rcmapsite.tpl.html',
            require: '^rcMap',
            scope: {
                name: '@'
            },
            link: function ($scope, $element, attrs, rcMapCtrl) {
                //var siteInfo = rcMapDataService.getSiteInfoByName($scope.name);
                var siteInfo = rcMapCtrl.getLevelElementInfo($scope.name);
                if (!siteInfo) {
                    $log.error($scope.name + ' not found');
                    return;
                }

                $scope.data = siteInfo;

                $scope.gotoLink = function () {
                    if (!$scope.data.link) {
                        return;
                    }
                    $window.location.href = $scope.data.link;
                }
                if ($scope.data.position) {
                    $element.css({top: $scope.data.position.y + 'px', left: $scope.data.position.x + 'px'});
                }
                if ($scope.data.color && $scope.data.color.back) {
                    $element.css('backgroundColor', $scope.data.color.back);
                }
                if ($scope.data.color && $scope.data.color.front) {
                    $element.css('color', $scope.data.color.front);
                }

                $scope.gotoLevel = function () {
                    rcMapCtrl.viewLevelDetail($scope.name);
                }
            }
        }
    }]);

