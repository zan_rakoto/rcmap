'use strict';

angular.module('rcMap')
    .directive('rcMapBuilding', [ 'rcMapDataService', '$window', '$log', function (rcMapDataService, $window, $log) {
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'components/map/directives/sites/buildings/rcmapbuilding.tpl.html',
            require: '^rcMap',
            scope: {
                name: '@'
            },
            link: function ($scope, $element, $attrs, rcMapCtrl) {
                var elInfo = rcMapCtrl.getLevelElementInfo($scope.name);
                if (!elInfo) {
                    $log.error($scope.name + ' not found');
                    return;
                }
                $scope.data = elInfo;

                $scope.gotoLink = function () {
                    if (!$scope.data.link) {
                        return;
                    }
                    $window.location.href = $scope.data.link;
                }
                if ($scope.data.position) {
                    $element.css({top: $scope.data.position.y + 'px', left: $scope.data.position.x + 'px'});
                }
                if ($scope.data.color && $scope.data.color.back) {
                    $element.css('backgroundColor',$scope.data.color.back);
                }
                if ($scope.data.color && $scope.data.color.front) {
                    $element.css('color', $scope.data.color.front);
                }

                $scope.gotoLevel = function(){
                    rcMapCtrl.viewLevelDetail($scope.name);
                }
            }
        }
    }]);

