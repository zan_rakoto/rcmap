'use strict';

angular.module('rcMap')
    .directive('rcMapNode', [ '$window', '$log', 'rcMapDataService', function ($window, $log, rcMapDataService) {
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'components/map/directives/sites/buildings/rcmapnode.tpl.html',
            require: '^rcMap',
            scope: {
                name: '@'
            },
            link: function ($scope, $element, $attrs, rcMapCtrl) {

                var elInfo = rcMapDataService.getNodeInfoByName($scope.name);
                if (!elInfo) {
                    $log.error($scope.name + ' not found');
                    return;
                }
                $scope.data = elInfo;



                $scope.gotoLink = function () {
                    if (!$scope.data.link) {
                        return;
                    }
                    $window.location.href = $scope.data.link;
                }
                if ($scope.data.position) {
                    $element.css({top: $scope.data.position.y + 'px', left: $scope.data.position.x + 'px'});
                }
                if ($scope.data.color && $scope.data.color.back) {
                    $element.css('backgroundColor', $scope.data.color.back);
                }
                if ($scope.data.color && $scope.data.color.front) {
                    $element.css('color', $scope.data.color.front);
                }
            }
        }
    }]);

