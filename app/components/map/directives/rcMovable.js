'use strict';

angular.module('rcMap')
    .constant('Hammer', Hammer)
    .constant('$M', $M)
    .constant('hammerConfigMovable', {
        transformMinScale: 1,
        transformMinRotation: 0,
        dragBlockHorizontal: true,
        dragBlockVertical: true,
        dragMinDistance: 0
    })
    .directive('rcMovable', ['Hammer', 'hammerConfigMovable', 'rcTransformService', '$M', '$log', function (Hammer, hammerConfigMovable, rcTransformService, $M, $log) {
        return{
            restrict: 'A',
            link: function ($scope, $element, $attrs) {

                var posX = 0, posY = 0,
                    lastPosX = 0, lastPosY = 0,
                    scale = 1, lastScale,
                    rotation = 0, lastRotation,
                    domElement = $element[0];

                var defaultOptions = {minX: 9, maxX: 509, minY: 143, maxY: 643};
                var options = angular.extend(defaultOptions, {minX: $attrs.minX, maxX: $attrs.maxX, minY: $attrs.minY, maxY: $attrs.maxY});

                var hammerObj = new Hammer(domElement, hammerConfigMovable);

                hammerObj.on('touch drag dragend transform', function (ev) {

                    ev.preventDefault();

                    if (ev.currentTarget !== ev.target)
                        return false;

                    switch (ev.type) {
                        case 'touch':
                            lastScale = scale;
                            lastRotation = rotation;
                            break;

                        case 'drag':
                            //$log.log('rcMovable move');
                            posX = ev.gesture.deltaX + lastPosX;
                            posY = ev.gesture.deltaY + lastPosY;
                            break;

                        case 'transform':
                            rotation = lastRotation + ev.gesture.rotation;
                            scale = Math.max(1, Math.min(lastScale * ev.gesture.scale, 10));
                            break;

                        case 'dragend':
                            lastPosX = posX;
                            lastPosY = posY;

                            break;
                    }
                    var mOld = rcTransformService.getCurrentMatrixTransform(domElement);




                    //var transform = rcTransformService.buildFullMatrixTransformCss(posX, posY, scale, rotation);
                    var mNew = rcTransformService.buildFullMatrixTransform(posX, posY, scale, rotation);
                    var matrix = $M([
                        [mOld.a, mOld.c, 0],
                        [mOld.b, mOld.d, 0],
                        [0, 0, 1]
                    ]).x(mNew);
                    var matrixCss = rcTransformService.buildMatrixTransformCssFromM(matrix);

                    rcTransformService.setElTransformStyle(domElement, matrixCss);
                });
            }
        }
    }
    ])
;
