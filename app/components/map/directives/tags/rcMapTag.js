'use strict';

angular.module('rcMap')
    .directive('rcMapTag', [ '$log', function ($log) {
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'components/map/directives/tags/rcmaptag.tpl.html',
            require: '^rcMap',
            scope: {
                name: '@'
            },
            link: function ($scope, $element) {

            }
        }
    }]);

