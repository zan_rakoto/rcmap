'use strict';

angular.module('rcMap')
    .directive('rcMap', [ 'rcMapDataService', 'rcTransformService', '$compile', '$window', '$document', '$log', function (rcMapDataService, rcTransformService, $compile, $window, $document, $log) {
        var total,
            currentScale = 1,
            originCoord;

        var loadLevelToScope = function (levelName, $scope) {

            var level;
            if (!levelName || levelName === 'root') {
                level = rcMapDataService.getRootLevel();
                total = level.nodes.length;
            }
            else {
                level = rcMapDataService.getLevelByName(levelName);
            }
            if (!level) {
                var errMsg = levelName + ' data not found'
                $log.error(errMsg);
                $window.alert(errMsg);
                return $scope.level;
            }

            $scope.currentPath = level.path;

            var nodes = level.nodes;
            var obj = level.obj;
            return {
                name: level.obj.name,
                total: total,
                moduleNbrs: nodes.length,
                elements: level.elems,
                nodes: nodes,
                tags: obj.tags,
                wallpaper: obj.wallpaper,
                elemsType: level.elemsType
            };
        }

        var zoom = function zoom($el, isZoomIn) {

            if (!$el) {
                return;
            }
            var el = $el[0];

            var step = 0.05;

            var scale = currentScale;
            scale *= isZoomIn ? 1 + step : 1 - step;

            if (scale < 0.3) {
                scale = currentScale;
            } else {
                currentScale = scale;
            }

            var reverse = scale < 0.7 ? 0.7 / scale : 1 / scale;

            var m = rcTransformService.getCurrentMatrixTransform(el);
            var tranformStyleValue = rcTransformService.buildMatrixTranslateScale(m.e, m.f, scale);
            rcTransformService.setElTransformStyle(el, tranformStyleValue);
            $el.find('.element').each(function () {
                var cssMatrixValue = rcTransformService.buildCssMatrixScale(reverse);
                rcTransformService.setElTransformStyle(this, cssMatrixValue);
            });

            return false;
        }

        var createViewPortElement = function ($scope, name, type, id) {
            var $newEl;
            if (type === 'sites') {
                var siteInfo = rcMapDataService.getSiteInfoByName(name);
                if (siteInfo.type === 'sites') {
                    $newEl = angular.element('<rc-map-site/>').attr('rc-draggable', '').attr('class', 'element site').attr('name', siteInfo.name);
                } else if (siteInfo.type === 'buildings') {
                    $newEl = angular.element('<rc-map-building/>').attr('rc-draggable', '').attr('class', 'element building').attr('name', siteInfo.name);
                } else if (siteInfo.type === 'floors') {
                    $newEl = angular.element('<rc-map-floor/>').attr('rc-draggable', '').attr('class', 'element floor').attr('name', siteInfo.name);
                } else if (siteInfo.type === 'rooms') {
                    $newEl = angular.element('<rc-map-room/>').attr('rc-draggable', '').attr('class', 'element room').attr('name', siteInfo.name);
                }
            }

            if (type === 'nodes') {
                $newEl = angular.element('<rc-map-node/>').attr('rc-draggable', '').attr('class', 'element node').attr('name', name);
            }

            if (type === 'tags' && id === 'pin') {
                var tagName = $window.prompt("Entrer le nom du tag", "Point de vérification");

                if (tagName) {
                    $scope.lastTagName = tagName;
                    $newEl = angular.element('<rc-map-tag/>').attr('rc-draggable', '').attr('class', 'element tag').data('name', tagName);
                }
            }

            return $newEl;
        }

        var resetZoom = function (el) {
            if (el.length && el.length > 0)
                el = el[0];
            rcTransformService.setElTransformStyle(el, 'matrix(1,0,0,1,0,0)');
        }

        var move = function (el, moveOptions) {

            if (el.length && el.length > 0)
                el = el[0];

            if (!moveOptions.d || !moveOptions.dir) return false;

            var rect = el.getBoundingClientRect();
            var w = rect.left + rect.width;
            var h = rect.top + rect.height;

            var d = moveOptions.dir === 'left' || moveOptions.dir === 'up' ? -1 * moveOptions.d : moveOptions.d;
            var current = rcTransformService.getCurrentMatrixTransform(el);


            /*
             if ((moveOptions.dir === 'left' || moveOptions.dir === 'right')
             && ((rect.left >= options.minX && d > 0) || (w <= options.maxX && d < 0)))
             d = 0;

             if ((moveOptions.dir === 'up' || moveOptions.dir === 'down')
             && ((rect.top >= options.minY && d > 0) || (h <= options.maxY && d < 0)))
             d = 0;

             if (d === 0) return false;
             */

            var moveMatrix = moveOptions.dir === 'up' || moveOptions.dir === 'down' ? $M([
                [1, 0, 0],
                [0, 1, d],
                [0, 0, 1]
            ]) : $M([
                [1, 0, d],
                [0, 1, 0],
                [0, 0, 1]
            ]);

            var movedMatrix = current.matrix.x(moveMatrix);
            rcTransformService.setElTransformStyle(el, rcTransformService.buildMatrixTransformCssFromM(movedMatrix));
            return false;
        }

        return{
            restrict: 'E',
            scope: {},
            replace: true,
            templateUrl: 'components/map/directives/rcmap.tpl.html',
            controller: function ($scope, $element) {

                var $vp = $element.find('#viewport');
                $vp.bind('dblclick', function () {
                    zoom($vp, true);
                });

                this.viewLevelDetail = function (levelName) {
                    $scope.level = loadLevelToScope(levelName, $scope);
                    //$log.log('Change level to ' + levelName);
                }

                this.getLevelElementInfo = function (elemName) {
                    if (!$scope.level)return;
                    var find,
                        els = $scope.level.elements;
                    for (var i = 0; i < els.length; i++) {
                        var el = els[i];
                        if (el.name.trim() === elemName.trim()) {
                            find = el;
                            break;
                        }
                    }
                    return find;
                }

                this.goUp = function () {

                    var previousLevel,
                        len = $scope.currentPath.length;

                    if (len < 2)
                        return;

                    previousLevel = $scope.currentPath[len - 2];
                    this.viewLevelDetail(previousLevel);
                }

                this.zoomIn = function () {
                    zoom($vp, true);
                }

                this.zoomOut = function () {
                    zoom($vp, false);
                }

                this.resetZoom = function () {
                    resetZoom($vp);
                }

                this.handleDrop = function (ev) {

                    var data = ev.data;
                    var $droppable = ev.droppable;
                    var fromActionZone = data.$el.parents('#action-zone').length > 0;
                    var fromMap = data.$el.parents('.map').length > 0;
                    if ($droppable.hasClass('viewport') && fromMap)
                        return;

                    // Handle drop in viewport
                    if ($droppable.hasClass('viewport') && fromActionZone) {

                        var vpRect = $droppable.offset();
                        var x = data.x - vpRect.left;
                        var y = data.y - vpRect.top;
                        var m = rcTransformService.getCurrentMatrixTransform($droppable[0]);
                        var matrixInv = m.matrix.inv();

                        var coordT = m.matrix.x($V([-originCoord.x, -originCoord.y, 1]));

                        var imT = $V([x + coordT.e(1) , y + coordT.e(2) , 1]);
                        var im = matrixInv.x(imT);
                        var om = {x: im.e(1) + originCoord.x, y: im.e(2) + originCoord.y};

                        var xReal = om.x;
                        var yReal = om.y;

                        var elName = data.$el.data('name');
                        var elType = data.$el.data('type');
                        var elId = data.$el.attr('id');

                        var $newEl = createViewPortElement($scope, elName, elType, elId);

                        /*var $newEl = data.$el.clone()
                         .removeClass('unset draggable')
                         .attr('rc-draggable', '')
                         .css({'position': 'absolute', 'margin': '0px', 'left': xReal + 'px', top: yReal + 'px'});*/

                        if (!$newEl) {
                            $log.error('Cannot create element name: %s type: %s in map', elName, elType);
                            return;
                        }

                        $newEl.css({'position': 'absolute', 'margin': '0px', 'left': xReal + 'px', top: yReal + 'px'});

                        $compile($newEl)($scope, function (cloned) {
                            $droppable.append(cloned);
                            var decomposedMatrix = rcTransformService.decomposeMatrix({a: m.a, b: m.b, c: m.c, d: m.d, e: m.e, f: m.f});
                            rcTransformService.setElTransformStyle(cloned[0], 'scale(' + 1 / decomposedMatrix.scaleX + ')');
                        });
                    }
                }

                this.getLastTagName = function () {
                    return $scope.lastTagName;
                }
            },
            link: function ($scope, $element, $attrs) {

                $scope.lastTagName = '';
                var $vp = $element.find('#viewport');
                var vp = $vp[0];

                //Get initial transform origin
                originCoord = rcTransformService.getCurrentMatrixTransformOrigin($vp[0]);

                $window.onkeypress = function (e) {
                    var keyCode = e.keyCode ? e.keyCode : e.which;
                    if (keyCode === 32) {
                        resetZoom($vp);
                        return false;
                    }
                };

                var onLevelChange = function (newValue, oldValue, scope) {

                    if (!newValue){
                        $log.error('Undefined new level data!');
                        return;
                    }
                    // Load level elements
                    var containerElements = angular.element('<div/>');
                    angular.forEach(newValue.elements, function (value) {
                            if (!value.position) {
                                return;
                            }
                            if (newValue.elemsType === 'sites') {
                                containerElements.append(
                                    angular.element('<rc-map-site/>')
                                        .attr('rc-draggable', '')
                                        .attr('class', 'element site')
                                        .attr('name', value.name));
                            } else if (newValue.elemsType === 'buildings') {
                                containerElements.append(angular.element('<rc-map-building/>')
                                    .attr('rc-draggable', '')
                                    .attr('class', 'element building')
                                    .attr('name', value.name));
                            } else if (newValue.elemsType === 'floors') {
                                containerElements.append(angular.element('<rc-map-floor/>')
                                    .attr('rc-draggable', '')
                                    .attr('class', 'element floor')
                                    .attr('name', value.name));
                            } else if (newValue.elemsType === 'rooms') {
                                containerElements.append(angular.element('<rc-map-room/>')
                                    .attr('rc-draggable', '')
                                    .attr('class', 'element room')
                                    .attr('name', value.name));
                            } else if (newValue.elemsType === 'nodes') {
                                containerElements.append(angular.element('<rc-map-node/>')
                                    .attr('rc-draggable', '')
                                    .attr('class', 'element node')
                                    .attr('name', value.name));
                            }
                        }
                    );
                    $compile(containerElements.html())($scope, function (cloned, $scope) {
                        $vp.empty();
                        $vp.append(cloned);
                    });

                    // Set background-image of the level
                    if (newValue.wallpaper) {
                        $vp.css('background-image', 'url("' + newValue.wallpaper + '")');
                    } else {
                        $vp.css('background-image', '');
                    }

                    // Load level tags
                    var tagContainer = angular.element('<div/>');
                    angular.forEach(newValue.tags, function (value) {
                            if (!value.position) {
                                return;
                            }
                            var tagEl = angular.element('<rc-map-tag/>').attr('rc-draggable', '').attr('class', 'element tag').attr('name', value.name);
                            tagEl.css({top: value.position.y + 'px', left: value.position.x + 'px'})
                            tagContainer.append(tagEl);
                        }
                    );
                    $compile(tagContainer.html())($scope, function (cloned) {
                        $vp.append(cloned);
                    });
                }

                $scope.level = loadLevelToScope('', $scope);
                $scope.$watch('level', onLevelChange, true);
            }
        }
    }]);