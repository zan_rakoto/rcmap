'use strict';

angular.module('rcMap')
    .directive('rcDroppable', [ '$log', 'rcDragAndDropService', function ($log, rcDragAndDropService) {
        return{
            restrict: 'A',
            scope: {},
            require: '^rcMap',
            link: function ($scope, $element, $attrs, rcMapCtrl) {
                rcDragAndDropService.registerDroppable($element)
                    .then(null, null, function (ev) {
                        //$log.log(ev);
                        if (ev.type === 'enter') {
                            //$log.log('enter');
                            $element.addClass("dragOver");
                        } else if (ev.type === 'drop') {
                            //$log.log('drop');

                            ev.droppable = $element;
                            rcMapCtrl.handleDrop(ev);

                            $element.removeClass("dragOver");
                        } else if (ev.type === 'exit') {
                            //$log.log('exit');
                            $element.removeClass("dragOver");
                        }
                    });
            }
        }
    }]);