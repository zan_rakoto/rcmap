'use strict';

angular.module('rcMap')
    .directive('rcMapActionZone', [ 'rcMapDataService', '$compile', '$log', function (rcMapDataService, $compile, $log) {
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'components/map/directives/actionszone/rcmapactionzone.tpl.html',
            require: '^rcMap',
            scope: false,
            link: function ($scope, $element, $attrs, rcMapCtrl) {

                var $lvlZa = $element.find('.levels'),
                    $nodeZa = $element.find('.nodes'),
                    $tagZa = $element.find('.tags');

                $scope.gotoParent = function (parentName) {
                    rcMapCtrl.viewLevelDetail(parentName);
                }

                var onLevelChange = function (newValue, oldValue, scope) {

                    if (!newValue){
                        $log.error('Undefined new level data!');
                        return;
                    }

                    //$log.log('Action zone: level changed to "' + newValue.name + '"');
                    //$log.log(oldValue);
                    //$log.log(newValue);

                    $lvlZa.empty();
                    $nodeZa.empty();
                    $tagZa.empty();

                    var ngLevelsElement = angular.element('<div/>'),
                        ngNodesElement = angular.element('<div/>'),
                        ngTagsElement = angular.element('<div/>').append(angular.element('<div/>')
                            .attr('data-type', 'tags')
                            .attr('class', 'tag element unset')
                            .attr('rc-draggable', '')
                            .attr('id', 'pin'));

                    //var level = $scope.level;
                    var level = newValue;
                    if (level.elemsType !== 'nodes') {
                        angular.forEach(level.elements, function (value) {
                                var draggableAttr = '';
                                var cssClass = 'site element';
                                if (value.position) {
                                    cssClass += ' set';
                                } else {
                                    cssClass += ' unset';
                                    draggableAttr = 'rc-draggable';
                                }
                                ngLevelsElement.append(angular.element('<div/>')
                                    .attr(draggableAttr || 'nodrag', '')
                                    .attr('data-name', value.name)
                                    .attr('data-type', 'sites')
                                    .text(value.name)
                                    .addClass(cssClass));
                            }
                        );
                        $compile(ngLevelsElement.html())($scope, function (cloned) {
                            $lvlZa.append(cloned);
                        });
                    }

                    angular.forEach(level.nodes, function (value) {
                            var draggableAttr = '';
                            var cssClass = 'node element';
                            if (value.position) {
                                cssClass += ' set';
                            } else {
                                cssClass += ' unset';
                                //if ($scope.level.name === value.parentName) // Uncomment if level cannot be put in other level than it is defined
                                draggableAttr = 'rc-draggable';
                            }
                            ngNodesElement.append(angular.element('<div/>')
                                .attr(draggableAttr || 'nodrag', '')
                                .attr('data-name', value.name)
                                .attr('data-type', 'nodes')
                                .text(value.name).addClass(cssClass));
                                //.attr('ng-click', 'gotoParent("' + value.parentName.trim() + '")')); // cf. above comment
                        }
                    );
                    $compile(ngNodesElement.html())($scope, function (cloned) {
                        $nodeZa.append(cloned);
                    });

                    angular.forEach(level.tags, function (value) {
                            var draggableAttr = '';
                            var cssClass = 'tag element';
                            if (value.position) {
                                cssClass += ' set';
                            } else {
                                cssClass += ' unset';
                                draggableAttr = 'rc-draggable';
                            }
                            ngTagsElement.append(angular.element('<div/>')
                                .attr(draggableAttr || 'nodrag', '')
                                .attr('data-name', value.name)
                                .attr('data-type', 'tags')
                                .text(value.name).addClass(cssClass));
                        }
                    );
                    $compile(ngTagsElement.html())($scope, function (cloned) {
                        $tagZa.append(cloned);
                    });
                }

                $scope.$watch('level', onLevelChange, true);

            }
        }
    }]);

