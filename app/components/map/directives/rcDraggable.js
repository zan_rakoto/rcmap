'use strict';

angular.module('rcMap')
    .constant('hammerConfigDraggable', {
        dragBlockHorizontal: true,
        dragBlockVertical: true,
        dragMinDistance: 0
    })
    .directive('rcDraggable', ['Hammer', 'hammerConfigDraggable', 'rcDragAndDropService', '$compile', '$log', function (Hammer, hammerConfigDraggable, rcDragAndDropService, $compile, $log) {
        return{
            restrict: 'A',
            require: '^rcMap',
            link: function ($scope, $element, $attrs, rcMapCtrl) {

                var posX = 0, posY = 0,
                    lastPosX = 0, lastPosY = 0,
                    domElement = $element[0];

                var hammerObj = new Hammer(domElement, hammerConfigDraggable);

                hammerObj.on('touch drag dragend', function (ev) {

                    ev.preventDefault();

                    if (ev.currentTarget !== ev.target)
                        return false;
                    var rect = $element.offset();
                    switch (ev.type) {
                        case 'touch':
                            lastPosX = rect.left;
                            lastPosY = rect.top;
                            return false;

                        case 'drag':
                            //$log.log('rcDraggable drag');
                            this.style.zIndex = '1000';
                            domElement.style.position = 'absolute';
                            posX = ev.gesture.deltaX + lastPosX;
                            posY = ev.gesture.deltaY + lastPosY;
                            $element.offset({left: posX, top: posY });
                            rcDragAndDropService.registerDragMouvement({y: posY, x: posX, el: this, $el: $(this), ev: ev });
                            return false;

                        case 'dragend':
                            this.style.position = '';
                            this.style.zIndex = '';
                            lastPosX = posX;
                            lastPosY = posY;
                            rcDragAndDropService
                                .registerDrop({y: posY, x: posX, el: this, $el: $(this), ev: ev  })
                                .then(function ($droppable) {

                                    if ($droppable[0] === $element.parent()[0])
                                        return;

                                    var clone = $element
                                        .clone()
                                        .removeClass('unset')
                                        .addClass('set')
                                        .removeAttr('rc-draggable')
                                        .appendTo($element.parent());

                                    if ($element.attr('id') === 'pin') {
                                        clone.text(rcMapCtrl.getLastTagName());
                                    } else {
                                        $element.remove();
                                    }
                                },
                                function () {
                                    if (posX < 0)
                                        posX = 0;
                                    if (posY < 0)
                                        posY = 0;
                                    $element.offset({left: posX, top: posY });
                                });
                            return false;
                    }
                });
            }
        }
    }
    ])
;
