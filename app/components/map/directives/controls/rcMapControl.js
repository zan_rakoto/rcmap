'use strict';

angular.module('rcMap')
    .directive('rcMapControl', [ '$log', function ($log) {
        return{
            restrict: 'E',
            replace: false,
            templateUrl: 'components/map/directives/controls/rcmapcontrol.tpl.html',
            require: '^rcMap',
            scope: true,
            link: function ($scope, $element, $attr, rcMapCtrl) {
                $scope.goUp = function(){
                    rcMapCtrl.goUp();
                };
                $scope.zoomIn = function(){
                    rcMapCtrl.zoomIn();
                };
                $scope.zoomOut = function(){
                    rcMapCtrl.zoomOut();
                };
            }
        }
    }]);

